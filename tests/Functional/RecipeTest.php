<?php

namespace Tests\Functional;

class RecipeTest extends BaseTestCase
{
    public function testGetAllRecipes()
    {
        $response = $this->runApp('GET', '/api/recipes');
        $this->assertEquals(200, $response->getStatusCode());
    }


}
