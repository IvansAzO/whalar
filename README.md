
# Whalar test PHP Developer

API developed by Iv�n Mu�oz Ruiz.
## Instructions to install docker container

#### Installing docker in ubuntu 18.04:

```shell
sudo apt-get install docker
sudo apt-get install docker-compose
```

## Instructions to install composer

#### Installing composer in ubuntu 18.04:
 via curl

```shell
sudo curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
```
Test installation
```shell
composer
```
And you should get a commands list.


## Instructions to clone repository (git)

at user's home __mkdir__ folder proyects(or choose your prefer name), inside it:
```shell
git clone https://IvansAzO@bitbucket.org/IvansAzO/whalar.git
```
This will create a folder with name __whalar__

## Installing packages
```shell
cd whalar
composer install
```

## Creating env file
```shell
cp .env.example .env
```
This will create .env file with settings values, the file .env.example file never uploads to the repository with data, but in this case I have done it so that it only has to be copied to .env

## Docker usage

__Up containers:__
By shell navigate to your folder __whalar__
Up containers in background
```shell
docker-compose up -d
```
if you get problem (elasticsearch exited with code 78), change the `vm.max_map_count` value to 262144 with this command in your local machine: `sudo sysctl -w vm.max_map_count=262144`.

## Instructions to open api (first time will load data automatically)

 1. Open your favorite browser
 2. Navigate to http://localhost:8080/api/recipes (remember first time will load automatically all data from json files, refresh again to see all recipes, it will not occurs more)
 3. To see the documentation visit http://localhost:8080/api/docs (Json documentation with swagger, It is not all documented)
 4. If you want you can visit http://0.0.0.0:5601 , to see kibana.

