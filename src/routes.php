<?php

use App\Controllers\RecipeController;
use Slim\Http\Request;
use Slim\Http\Response;

$app->group('/api',
    function () {
        // Recipes Routes
        $this->get('/recipes', RecipeController::class . ':index')->setName('recipe.index');
        $this->get('/recipes/{search}', RecipeController::class . ':show')->setName('recipe.show');
        $this->post('/recipes', RecipeController::class . ':store')->setName('recipe.store');
        $this->delete('/recipes', RecipeController::class . ':destroy')->setName('recipe.destroy');
        $this->put('/recipes',RecipeController::class . ':update')->setName('recipe.update');
        // Api documentation in JSON with swagger (Is not fully documentation, only controller methods)
        $this->get('/docs', function ($request, $response, $args) {
            $dir = __DIR__ . '/App/Controllers'; // Scan Controller folder
            $openapi = \OpenApi\scan([$dir]);
            $response->write($openapi->toJson());
            $response = $response->withHeader('Content-Type', 'application/json');
            return $response;
        });
    }
);
