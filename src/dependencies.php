<?php
use App\Exceptions\ErrorHandler;

$container = $app->getContainer();

// Error Handler
$container['errorHandler'] = function ($c) {
    return new ErrorHandler($c['settings']['displayErrorDetails']);
};


// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Request Validator
$container['validator'] = function ($c) {
    \Respect\Validation\Validator::with('\\App\\Validation\\Rules');
    return new \App\Validation\Validator();
};
