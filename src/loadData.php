<?php
use Elasticsearch\ClientBuilder;
use Basemkhirat\Elasticsearch\Connection;

$hosts = [
    'http://elasticsearch:9200',         // IP + Port
];
$client = ClientBuilder::create()           // Instantiate a new ClientBuilder
    ->setHosts($hosts)      // Set the hosts
    ->build();

$indexParams['index'] = 'recipes';

// If not exists index, create it and fill data
if ( !$client->indices()->exists($indexParams) ) {
    $directory = 'recipes';
    $scannedDirectory = array_diff(scandir($directory), array('..', '.'));

    foreach($scannedDirectory as $file) {
        $c++;
        $recipe = file_get_contents('recipes/'.$file);

        $params['body'][] = [
            'index' => [
                '_index' => 'recipes',
                '_type' => 'recipes',
            ]
        ];

        $params['body'][] = json_decode($recipe, true);

    }

    $responses = $client->bulk($params);
}
