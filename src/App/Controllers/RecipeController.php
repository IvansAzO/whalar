<?php
namespace App\Controllers;

use App\Models\Recipe;
use Slim\Http\Request;
use Slim\Http\Response;
use Respect\Validation\Validator as v;
use Interop\Container\ContainerInterface;
/**
 * @OA\Info(title="Whalar test", version="0.1")
 */
class RecipeController
{

    /** @var \App\Validation\Validator */
    protected $validator;

    /**
     * RecipeController constructor.
     *
     * @param \Interop\Container\ContainerInterface $container
     *
     * @internal param $auth
     */
    public function __construct(ContainerInterface $container)
    {
        $this->validator = $container->get('validator');
    }


    /**
     * @OA\Get(path="/api/recipes", operationId="index",
     *   @OA\Response(response="200", description="All recipes")
     * )
     */
    /**
     * Return List of Recipes
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param array               $args
     *
     * @return \Slim\Http\Response
     */
    public function index(Request $request, Response $response, array $args) : Response
    {
        $recipe = new Recipe();
        $recipes = $recipe->allQuery();
        return $response->withJson(['recipes' => $recipes]);
    }


    /**
     * @OA\Get(path="/api/recipes/{search}", operationId="show",
     *   @OA\Response(response="200", description="Recipes thant contain search")
     * )
     */
    /**
     * Return searched Recipes
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param array               $args
     *
     * @return \Slim\Http\Response
     */
    public function show(Request $request, Response $response, array $args) : Response
    {

        $search = $args['search'];
        $recipe = new Recipe();
        $recipes = $recipe->searchQuery($search);

        return $response->withJson(['recipes' => $recipes]);
    }


    /**
     * @OA\Post(
     *     path="/api/recipes",
     *     summary="Adds a new recipe",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="ingredients",
     *                     description="ingredients separated by ,",
     *                     type="array",
     *                     @OA\Items(
     *                         type="array",
     *                          @OA\Items()
     *                       ),
     *                 ),
     *                 @OA\Property(
     *                     property="directions",
     *                     description="ingredients separated by ,",
     *                     type="array",
     *                     @OA\Items(
    *                          type="array",
    *                               @OA\Items()
    *                      ),
     *                 ),
     *                 @OA\Property(
     *                     property="prep_time_min",
     *                     type="int"
     *                 ),
     *                 @OA\Property(
     *                     property="cook_time_min",
     *                     type="int"
     *                 ),
     *                 @OA\Property(
     *                     property="servings",
     *                     type="int"
     *                 ),
     *                 @OA\Property(
     *                     property="tags",
     *                     description="ingredients separated by ,",
     *                     type="array",
     *                     @OA\Items(
     *                        type="array",
     *                        @OA\Items()
     *                     ),
     *                 ),
     *                 @OA\Property(
     *                     description="author in json object with name and url",
     *                     property="author",
     *                     type="object"
     *                 ),
     *                 @OA\Property(
     *                     property="source_url",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
     /**
     * Store Recipe
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param array               $args
     *
     * @return \Slim\Http\Response
     */
    public function store(Request $request, Response $response, array $args) : Response
    {
        $data = $request->getParams();

        $this->validator->validateArray($data,
        [
            'title'       => v::notEmpty(),
            'description' => v::notEmpty(),
            'ingredients' => v::notEmpty(),
            'directions' => v::notEmpty(),
            'prep_time_min' => v::optional(v::intVal()),
            'cook_time_min' => v::optional(v::intVal()),
            'servings' => v::intVal(),
            'tags' => v::notEmpty(),
            'author' => v::json(),
            'source_url' => v::notEmpty(),
        ]);

        if ($this->validator->failed()) {
            return $response->withJson(['errors' => $this->validator->getErrors()], 422);
        }
        // Create Recipe object
        $recipe = new Recipe();
        $recipe->create($data);
        // Save on elasticSearch
        $recipes = $recipe->createQuery();

        return $response->withJson(['recipe' => $recipes]);
    }



    /**
     * @OA\Put(
     *     path="/api/recipes",
     *     summary="Edit one recipe by id",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="id",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="title",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="ingredients",
     *                     description="ingredients separated by ,",
     *                     type="array",
     *                     @OA\Items(
     *                         type="array",
     *                          @OA\Items()
     *                       ),
     *                 ),
     *                 @OA\Property(
     *                     property="directions",
     *                     description="ingredients separated by ,",
     *                     type="array",
     *                     @OA\Items(
    *                          type="array",
    *                               @OA\Items()
    *                      ),
     *                 ),
     *                 @OA\Property(
     *                     property="prep_time_min",
     *                     type="int"
     *                 ),
     *                 @OA\Property(
     *                     property="cook_time_min",
     *                     type="int"
     *                 ),
     *                 @OA\Property(
     *                     property="servings",
     *                     type="int"
     *                 ),
     *                 @OA\Property(
     *                     property="tags",
     *                     description="ingredients separated by ,",
     *                     type="array",
     *                     @OA\Items(
     *                        type="array",
     *                        @OA\Items()
     *                     ),
     *                 ),
     *                 @OA\Property(
     *                     property="author",
     *                     description="author in json object with name and url",
     *                     type="object"
     *                 ),
     *                 @OA\Property(
     *                     property="source_url",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    /**
     * Update Recipe by id
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param array               $args
     *
     * @return \Slim\Http\Response
     */
    public function update(Request $request, Response $response, array $args) : Response
    {
        $data = $request->getParams();
        // Only is necessary id, because can add other fields news
        $this->validator->validateArray($data,
        [
            'id' => v::notEmpty(),
        ]);

        if ($this->validator->failed()) {
            return $response->withJson(['errors' => $this->validator->getErrors()], 422);
        }

        $recipe = new Recipe();
        $recipe->create($data);
        // update on elasticsearch
        $recipes = $recipe->updateQuery();

        return $response->withJson(['recipes' => $recipes]);
    }


    /**
     * @OA\Delete(
     *     path="/api/recipes",
     *     summary="Delete one recipe by id",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="id",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */

     /**
     * Delete Recipe by id
     *
     * @param \Slim\Http\Request  $request
     * @param \Slim\Http\Response $response
     * @param array               $args
     *
     * @return \Slim\Http\Response
     */
    public function destroy(Request $request, Response $response, array $args) : Response
    {
        $data = $request->getParams();
        $this->validator->validateArray($data,
        [
            'id'       => v::notEmpty(),
        ]);

        if ($this->validator->failed()) {
            return $response->withJson(['errors' => $this->validator->getErrors()], 422);
        }
        $recipe = new Recipe();
        $recipe->create($data);
        $recipes = $recipe->deleteQuery();

        return $response->withJson(['recipes' => $recipes]);
    }



}
