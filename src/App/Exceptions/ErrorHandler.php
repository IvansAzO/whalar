<?php
namespace App\Exceptions;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\Error;
use Slim\Handlers\NotFound;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Elasticsearch\Common\Exceptions\RuntimeException;
use App\Exceptions\Custom\EmptyIdParameterException;

class ErrorHandler extends Error
{
    /** @inheritdoc */
    public function __construct(bool $displayErrorDetails)
    {
        parent::__construct($displayErrorDetails);
    }

    /** @inheritdoc */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, \Exception $exception)
    {
        if ($exception instanceof Missing404Exception) {
            return $response->withJson(['recipe' => 'Missing 404 Exception'], $exception->getCode());
        } elseif ($exception instanceof EmptyIdParameterException) {
            return $response->withJson(['recipe' => $exception->getMessage()], $exception->getCode());
        }
        return parent::__invoke($request, $response, $exception);
    }
}
