<?php

namespace App\Exceptions\Custom;

/**
 * Custom exception Empty id
 */
class EmptyIdParameterException extends \Exception
{
}
