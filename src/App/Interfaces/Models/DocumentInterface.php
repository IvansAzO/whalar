<?php

namespace App\Interfaces\Models;

interface DocumentInterface
{
    /**
     * Return params name in fields array
     *
     */
    public function getParamValue(string $paramName);

}
