<?php

namespace App\Models;
use Elasticsearch\ClientBuilder;
use App\Interfaces\Models\DocumentInterface;

class Recipe extends ElasticSearch implements DocumentInterface
{

    public $recipe;

    /**
     * Documents data index and type
     *
     * @var array
     */
    protected $recipeParams = [
        'index' => 'recipes',
        'type' => 'recipes',
    ];

    public $fields = [];

    public function __construct(array $data = [])
    {
        parent::__construct($this->recipeParams);
    }

    public function create(array $data)
    {
        $this->fields['id'] = !empty($data['id']) ? $data['id'] : '';
        $this->fields['title'] = $data['title'];
        $this->fields['description'] = $data['description'];
        $this->fields['ingredients'] = explode(',', $data['ingredients']);
        $this->fields['directions'] =  explode(',', $data['directions']);
        $this->fields['prep_time_min'] = !empty($data['prep_time_min']) ? intval($data['prep_time_min']) : null;
        $this->fields['cook_time_min'] = !empty($data['cook_time_min']) ? intval($data['cook_time_min']) : null;
        $this->fields['servings'] = intval($data['servings']);
        $this->fields['tags'] = explode(',', $data['tags']);
        $this->fields['author'] = json_decode($data['author']);
        $this->fields['source_url'] = $data['source_url'];
    }

    /**
     * {@inheritdoc}
     */
    public function getParamValue(string $paramName)
    {
        return $this->fields[$paramName];
    }
}
