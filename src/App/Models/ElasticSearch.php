<?php

namespace App\Models;

use Elasticsearch\ClientBuilder;
use App\Exceptions\Custom\EmptyIdParameterException;
use Interop\Container\ContainerInterface;

class ElasticSearch
{
    protected $client;
    protected $params;
    private $elasticHost;

    protected function __construct(array $params)
    {
        $this->params = [
            'index' => $params['index'],
            'type' => $params['type']
        ];
        $this->elasticHost = [
            getenv('ES_TEST_HOST')
        ];

        $this->client = ClientBuilder::create()->setHosts($this->elasticHost)->build();
    }


    /**
     * Insert int elasticsearch
     * @return array
     */
    public function createQuery() : array
    {
        $data = $this->fields;
        $dataFiltered = $this->cleanData($data);
        $this->params['body'] = $dataFiltered;
        $response = $this->client->index($this->params);
        return $response;
    }

    /**
     * Get all documents from elasticsearch
     * @return array
     */
    public function allQuery() : array
    {
        return $this->client->search($this->params);
    }

    /**
     * Search in all fields in elasticsearch
     * @return array
     */
    public function searchQuery($search) : array
    {
        $this->params["body"] = [
            "query" => [
                "bool" => [
                    "should" => [
                        [
                            "query_string" => [
                                "query" => '*'.$search.'*',
                                "fields" => ["*"], // all fields
                                "lenient" => true,
                            ]
                        ],
                    ]
                ]
            ]
        ];

        $results = $this->client->search($this->params);
        return $this->client->search($this->params);
    }

    /**
     * Delete by id in elasticsearch
     * @return array
     */
    public function deleteQuery() : array
    {
        $this->params['id'] = $this->fields['id'];
        $response = $this->client->delete($this->params);
        return $response;
    }


    /**
     * Update in elasticsearch
     * @return array
     */
    public function updateQuery() : array
    {
        $data = $this->fields;
        if (!empty($data['id'])) {
            $this->params['id'] = $data['id'];
            // Remove id from array data, id only use to search
            $dataFiltered = $this->cleanData($data);
            $this->params['body']['doc'] = $dataFiltered;
            $response = $this->client->update($this->params);
            return $response;
        }
        throw new EmptyIdParameterException('Field id is required', 400);
    }

    private function cleanData(array $data) : array
    {
        unset($data['id']); // id will generate automatic
        $dataFiltered = array_filter($data);
        return $dataFiltered;
    }
}
